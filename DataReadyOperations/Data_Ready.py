from  Xpath import Xpath
from DataFetchOperations import  Number_Of_Trades,PriceChangeAndTradedVolume
from FolderOperations import CopyFileOperation
import  time
class DataReadyOperation:
    def __init__(self):
        self.deliveryQty_list = None

    def MakeDataReady(self):
        #Object Declaration for URL Paths ,Xpaths
        url_object = Xpath.url()
        folder_object = Xpath.FolderPath()
        xpath_object = Xpath.Xpath()

        #Object Declaration for Operations related to fetching of data
        Number_Of_Trades_object = Number_Of_Trades.NumberOfTrades()
        PriceChange_object = PriceChangeAndTradedVolume.PriceChangeandTradedVolume()
        copyfile_object = CopyFileOperation.CopyFile()

        #Actual Operation
        PriceChange_object.Operation_To_Download_Data_For_Price_And_TradedVolume(url_object.URL_to_Fetch_Data_for_Price_and_TradedVolume,xpath_object.X_path_To_Select_FO_Stocks_FromDropDown,xpath_object.X_path_For_DownloadXSV_To_Get_Price_and_TradedVolume)
        copyfile_object.Operation_To_Copy_File_From_One_Folder_To_Another_Folder(folder_object.file_Download_Folder,folder_object.Folder_For_Price_and_TradedVolume_csv)
        self.deliveryQty_list = Number_Of_Trades_object.Opeartion_To_Fetch_Data_For_Number_of_Trades(url_object.URL_to_Fetch_Data_for_NumberOfTrades,xpath_object.X_path_for_Equities_BhavCopy,xpath_object.X_path_for_Security_wise_DeliveryPosition,xpath_object.X_path_for_Derivtives_BhavCopy,xpath_object.X_path_for_Derivtives_Label,folder_object.Folder_For_NumberOfTrades_zip,folder_object.file_Download_Folder,xpath_object.X_path_For_Body_text_SecurityWise)

