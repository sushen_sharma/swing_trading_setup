import pandas as pd
import matplotlib.pyplot as plt


def Average(lst):
    return sum(lst)/len(lst)


dataFrame = pd.read_csv("book2.csv")

filter_dataFrame = dataFrame[dataFrame['Series'] == 'EQ']
filter_dataFrame.rename(columns={'% Dly Qt to Traded Qty':'Percentage_delivery_qty'},inplace=True)
#print(filter_dataFrame.columns)
nt_tq = dataFrame["Total Traded Quantity"]/dataFrame["No. of Trades"]
filter_dataFrame['NT/TQ']=nt_tq
#print(filter_dataFrame.columns)
Actual_dataFrame = filter_dataFrame[["Average Price","Total Traded Quantity","Turnover","No. of Trades","Deliverable Qty","Percentage_delivery_qty","NT/TQ"]]
print(Actual_dataFrame.head())
Reverse_Data_Frame = Actual_dataFrame.iloc[::-1]


Average_Price_List = Reverse_Data_Frame["Average Price"].tolist()
TotalTradedQuantity_List = Reverse_Data_Frame["Total Traded Quantity"].tolist()
TurnOver_List = Reverse_Data_Frame["Turnover"].tolist()
NumberOfTrades_List = Reverse_Data_Frame["No. of Trades"].tolist()
DeliverableQuantity_List = Reverse_Data_Frame["Deliverable Qty"].tolist()
Percentage_Delivery_Qty_List = Reverse_Data_Frame["Percentage_delivery_qty"].tolist()
TQ_NT_List = Reverse_Data_Frame["NT/TQ"].tolist()


Average_Price_3Day_Average = Average(Average_Price_List[:3])
Average_Price_5Day_Average = Average(Average_Price_List[:5])
Average_Price_8Day_Average = Average(Average_Price_List[:8])
Average_Price_13Day_Average = Average(Average_Price_List[:13])

TotalTradedQuantity_3Day_Average = Average(TotalTradedQuantity_List[:3])
TotalTradedQuantity_5Day_Average = Average(TotalTradedQuantity_List[:5])
TotalTradedQuantity_8Day_Average = Average(TotalTradedQuantity_List[:8])
TotalTradedQuantity_13Day_Average = Average(TotalTradedQuantity_List[:13])


TurnOver_3Day_Average = Average(TurnOver_List[:3])
TurnOver_5Day_Average = Average(TurnOver_List[:5])
TurnOver_8Day_Average = Average(TurnOver_List[:8])
TurnOver_13Day_Average = Average(TurnOver_List[:13])

NumberOfTrades_3Day_Average = Average(NumberOfTrades_List[:3])
NumberOfTrades_5Day_Average = Average(NumberOfTrades_List[:5])
NumberOfTrades_8Day_Average = Average(NumberOfTrades_List[:8])
NumberOfTrades_13Day_Average = Average(NumberOfTrades_List[:13])


DeliverableQuantity_3Day_Average = Average(DeliverableQuantity_List[:3])
DeliverableQuantity_5Day_Average = Average(DeliverableQuantity_List[:5])
DeliverableQuantity_8Day_Average = Average(DeliverableQuantity_List[:8])
DeliverableQuantity_13Day_Average = Average(DeliverableQuantity_List[:13])


Percentage_Delivery_Qty_3Day_Average = Average(Percentage_Delivery_Qty_List[:3])
Percentage_Delivery_Qty_5Day_Average = Average(Percentage_Delivery_Qty_List[:5])
Percentage_Delivery_Qty_8Day_Average = Average(Percentage_Delivery_Qty_List[:8])
Percentage_Delivery_Qty_13Day_Average = Average(Percentage_Delivery_Qty_List[:13])


TQ_NT_3Day_Average = Average(TQ_NT_List[:3])
TQ_NT_5Day_Average = Average(TQ_NT_List[:5])
TQ_NT_8Day_Average = Average(TQ_NT_List[:8])
TQ_NT_13Day_Average = Average(TQ_NT_List[:13])

if TQ_NT_3Day_Average > TQ_NT_5Day_Average > TQ_NT_8Day_Average > TQ_NT_13Day_Average:
    print("Big Players are Active in this Stock")
else:
    print("Big Players are not Active in this Stock")


#Price increase in this order 3>5>8>13
#Traded Quantity increase in this order 3>5>8>13
#Turnover increase in this order
#Then it is delivery based buying

#If other wise ,it is delivery based selling
