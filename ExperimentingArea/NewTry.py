import  pandas as pd
#datasource = pd.read_csv("28SwingTradingSetup_Sheet.csv")

datasource = pd.read_csv("Prepared_Sheet.csv")
#datasource = datasource.head(650)
df_staging = pd.read_csv("Staging.csv")

datasource['p1'] = datasource['p2']
datasource['p2'] = df_staging["p_staging"]

datasource['nt1'] =  datasource['nt2']
datasource['nt2'] =  datasource['nt3']
datasource['nt3'] =  df_staging["nt_staging"]

datasource['tv1'] = datasource['tv2']
datasource['tv2'] = datasource['tv3']
datasource['tv3'] = df_staging["tv_staging"]

datasource['dq1'] = datasource['dq2']
datasource['dq2'] = datasource['dq3']
datasource['dq3'] = df_staging["dq_staging"]

datasource['coi1'] = datasource['coi2']
datasource['coi2'] = df_staging["coi_staging"]

datasource['oic1'] = datasource['oic2']
datasource['oic2'] = datasource['oic3']
datasource['oic3'] = df_staging["oic_staging"]

#datasource.to_csv("MadeNewSheet.csv")

datasource['nt3'] = datasource['nt3'].str.replace(',','').astype(float)



datasource['%cnt1'] = datasource['nt2'] - datasource['nt1']
datasource['%cnt2'] = datasource['nt3'] - datasource['nt2']

# datasource['tv1'] = datasource['tv1'].str.replace(',','').astype(float)
# datasource['tv2'] = datasource['tv2'].str.replace(',','').astype(float)
# datasource['tv3'] = datasource['tv3'].str.replace(',','').astype(float)

datasource['%ctv1'] = datasource['tv3'] - datasource['tv2']
datasource['%ctv2'] = datasource['tv2'] - datasource['tv1']

datasource['%cdq1'] = datasource['dq3'] - datasource['dq2']
datasource['%cdq2'] = datasource['dq2'] - datasource['dq1']

datasource['%coi1'] = datasource['oic3'] - datasource['oic2']
datasource['%coi2'] = datasource['oic3'] - datasource['oic1']

datasource.to_csv("MadeNewSheet.csv")
print("sheet Written")