from DataReadyOperations import Data_Ready
from Xpath import  Xpath
import  pandas as pd
import os

#variable Declarations
list_PriceVolumeFiles = []
list_TradedVolume = []


#Object Declaration
Data_Ready_object = Data_Ready.DataReadyOperation()

Xpath_object = Xpath.FolderPath()

#First Phase Make Data Ready after fetching all Data Sheets from Web Sources and Collecting it as CSV format in respective folders
Data_Ready_object.MakeDataReady()
for root,dirs,files in os.walk(Xpath_object.Folder_For_Price_and_TradedVolume_csv):
    for filename in files:
        list_PriceVolumeFiles.append(Xpath_object.Folder_For_Price_and_TradedVolume_csv+"\\"+filename)

if(len(list_PriceVolumeFiles) == 1):

    df_PriceChange_andTradedVolume_complete = pd.read_csv(list_PriceVolumeFiles[0])
    df_PriceChange_andTradedVolume= df_PriceChange_andTradedVolume_complete[['Symbol','Change','Traded Value(crs)']]

else:
    print("You have Extra files in Folder . Please remove them to Continue")


for root,dirs,files in os.walk(Xpath_object.Folder_For_NumberOfTrades_CSV):
    for filename in files:
        list_TradedVolume.append(Xpath_object.Folder_For_NumberOfTrades_CSV+"\\"+filename)

if(len(list_TradedVolume) == 2):

    df_Number_of_Trades_complete = pd.read_csv(list_TradedVolume[0])
    df_Number_of_Trades= df_Number_of_Trades_complete[['SYMBOL','TOTALTRADES']]
    #print(df_Number_of_Trades)

    df_Futures = pd.read_csv(list_TradedVolume[1])
    df_Contracts_and_OIchange_complete = df_Futures[(df_Futures['INSTRUMENT'] == 'FUTSTK') & (df_Futures['EXPIRY_DT'] == '28-Nov-2019')]


    df_Contracts_and_OIchange = df_Contracts_and_OIchange_complete[['SYMBOL','CONTRACTS','CHG_IN_OI']]
    df_DelieveryQty = pd.DataFrame(Data_Ready_object.deliveryQty_list,columns=["Symbol","DeliveryQty"])
  #  print (df_DelieveryQty["Symbol"])

else:
    print("You have Extra files in Folder . Please remove them to Continue")
#########################################################################################################33
#
# df_full_sheet = pd.DataFrame(columns=["symbol","p1","p2","nt1","nt2","nt3","%c1","%c2","tv1","tv2","tv3","%c1","%c2","dq1","dq2","dq3","%c1","%c2","coi1","coi2","oic1","oic2","oic3","%c1","%c2"])
# df_staging =  pd.DataFrame(columns=["symbol_staging","p_staging","nt_staging","tv_staging","dq_staging","coi_staging","oic_staging"])
#
# df_trial = pd.merge(df_Number_of_Trades,df_Contracts_and_OIchange,on="SYMBOL",how="left")
# df_PriceChange_andTradedVolume.rename(columns={'Symbol':'SYMBOL'},inplace=True)
# df_DelieveryQty.rename(columns={'Symbol':'SYMBOL'},inplace=True)
# df_DelieveryQty.to_csv("Delivery_Quantity.csv")
# df_trial = pd.merge(df_PriceChange_andTradedVolume,df_trial,on="SYMBOL",how="left")
# df_complete_data_fetched = pd.merge(df_DelieveryQty,df_trial,on="SYMBOL",how="left")
#
#
# df_complete_data_fetched = df_complete_data_fetched[df_complete_data_fetched['Change'].notnull()]
#
# df_staging["symbol_staging"] = df_complete_data_fetched["SYMBOL"]
# df_staging["p_staging"] = df_complete_data_fetched["Change"]
# df_staging["nt_staging"] = df_complete_data_fetched["Traded Value(crs)"]
# df_staging["tv_staging"] = df_complete_data_fetched["TOTALTRADES"]
# df_staging["dq_staging"] =df_complete_data_fetched["DeliveryQty"]
# df_staging["coi_staging"] = df_complete_data_fetched["CHG_IN_OI"]
# df_staging["oic_staging"] = df_complete_data_fetched["CONTRACTS"]
# df_staging.to_csv("Staging.csv")
#
# datasource = pd.read_csv("MadeNewSheet1.csv")
# df_staging = pd.read_csv("Staging.csv")
#
# datasource['p1'] = datasource['p2']
# datasource['p2'] = df_staging["p_staging"]
#
# datasource['nt1'] =  datasource['nt2']
# datasource['nt2'] =  datasource['nt3']
# datasource['nt3'] =  df_staging["nt_staging"]
#
# datasource['tv1'] = datasource['tv2']
# datasource['tv2'] = datasource['tv3']
# datasource['tv3'] = df_staging["tv_staging"]
#
# datasource['dq1'] = datasource['dq2']
# datasource['dq2'] = datasource['dq3']
# datasource['dq3'] = df_staging["dq_staging"]
#
# datasource['coi1'] = datasource['coi2']
# datasource['coi2'] = df_staging["coi_staging"]
#
# datasource['oic1'] = datasource['oic2']
# datasource['oic2'] = datasource['oic3']
# datasource['oic3'] = df_staging["oic_staging"]
#
# #datasource.to_csv("MadeNewSheet.csv")
#
# datasource['nt3'] = datasource['nt3'].str.replace(',','').astype(float)
#
#
#
# datasource['%cnt1'] = datasource['nt2'] - datasource['nt1']
# datasource['%cnt2'] = datasource['nt3'] - datasource['nt2']
#
# # datasource['tv1'] = datasource['tv1'].str.replace(',','').astype(float)
# # datasource['tv2'] = datasource['tv2'].str.replace(',','').astype(float)
# # datasource['tv3'] = datasource['tv3'].str.replace(',','').astype(float)
#
# datasource['%ctv1'] = datasource['tv3'] - datasource['tv2']
# datasource['%ctv2'] = datasource['tv2'] - datasource['tv1']
#
# datasource['%cdq1'] = datasource['dq3'] - datasource['dq2']
# datasource['%cdq2'] = datasource['dq2'] - datasource['dq1']
#
# datasource['%coi1'] = datasource['oic3'] - datasource['oic2']
# datasource['%coi2'] = datasource['oic3'] - datasource['oic1']
#
# datasource.to_csv("MadeNewSheet2.csv")
# print("sheet Written")
#
#
#
#
#
#
# # #datasource = datasource.head(650)
# # df_staging.to_csv("Staging.csv")
# #
# # datasource['p1'] = datasource['p2']
# # datasource['p2'] = df_staging["p_staging"]
# #
# # datasource['nt1'] =  datasource['nt2']
# # datasource['nt2'] =  datasource['nt3']
# # datasource['nt3'] =  df_staging["nt_staging"]
# #
# # datasource['tv1'] = datasource['tv2']
# # datasource['tv2'] = datasource['tv3']
# # datasource['tv3'] = df_staging["tv_staging"]
# #
# # datasource['dq1'] = datasource['dq2']
# # datasource['dq2'] = datasource['dq3']
# # datasource['dq3'] = df_staging["dq_staging"]
# #
# # datasource['coi1'] = datasource['coi2']
# # datasource['coi2'] = df_staging["coi_staging"]
# #
# # datasource['oic1'] = datasource['oic2']
# # datasource['oic2'] = datasource['oic3']
# # datasource['oic3'] = df_staging["oic_staging"]
# #
# # # datasource['%cnt1'] = datasource['nt2'] - datasource['nt1']
# # # datasource['%cnt2'] = datasource['nt3'] - datasource['nt2']
# # #
# # # datasource['tv1'] = datasource['tv1'].str.replace(',','').astype(float)
# # # datasource['tv2'] = datasource['tv2'].str.replace(',','').astype(float)
# # # datasource['tv3'] = datasource['tv3'].str.replace(',','').astype(float)
# # #
# # # datasource['%ctv1'] = datasource['tv3'] - datasource['tv2']
# # # datasource['%ctv2'] = datasource['tv2'] - datasource['tv1']
# # #
# # # datasource['%cdq1'] = datasource['dq3'] - datasource['dq2']
# # # datasource['%cdq2'] = datasource['dq2'] - datasource['dq1']
# # #
# # # datasource['%coi1'] = datasource['oic3'] - datasource['oic2']
# # # datasource['%coi2'] = datasource['oic3'] - datasource['oic1']
# #
# # datasource.to_csv("Final_Prepared_Sheet.csv")
# # print("sheet Written")