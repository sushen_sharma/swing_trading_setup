class url:
    URL_To_Fetch_Data_For_Equity = "https://www.nseindia.com/products/content/equities/equities/eq_security.htm"

class Xpath:
    Xpath_For_DateRange_Selector        =             "//select[@id='dateRange']"
    Xpath_For_Selecting_1Month          =             "//select[@id='dateRange']/option[@value='1month']"
    Xpath_For_Symbol_Input              =             "//input[@id='symbol']"
    Xpath_For_GetData_Button            =             "//img[@class='getdata-button']"
    Xpath_For_DataLink_Download         =             "//span[@class='download-data-link']"