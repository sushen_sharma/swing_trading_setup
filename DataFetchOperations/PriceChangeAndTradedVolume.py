from selenium.webdriver import  Chrome
from selenium.webdriver.chrome.options import  Options
import time
from Xpath import Xpath
class PriceChangeandTradedVolume:
 def Operation_To_Download_Data_For_Price_And_TradedVolume(self,URL_to_Fetch_Data_for_Price_and_TradedVolume,X_path_To_Select_FO_Stocks_FromDropDown,X_path_For_DownloadXSV_To_Get_Price_and_TradedVolume):
    chromeDriverObject = Xpath.ChromDriver()
    browser = Chrome(executable_path=chromeDriverObject.ChromDriverPath)
    browser.get(URL_to_Fetch_Data_for_Price_and_TradedVolume)
    time.sleep(5)
    search_form = browser.find_element_by_xpath(X_path_To_Select_FO_Stocks_FromDropDown)
    search_form.click()
    time.sleep(5)
    link = browser.find_element_by_xpath(X_path_For_DownloadXSV_To_Get_Price_and_TradedVolume)
    link.click()
    time.sleep((5))
    browser.close()
