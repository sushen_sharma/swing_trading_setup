from selenium.webdriver import  Chrome
from selenium.webdriver.chrome.options import  Options
import time
import  pandas as pd
from FolderOperations import  CopyFileOperation
from FolderOperations import  Unzip_operation,Delete_Existing_Files
from Xpath import Xpath

class NumberOfTrades:
 def Opeartion_To_Fetch_Data_For_Number_of_Trades(self,URL_to_Fetch_Data_for_NumberOfTrades,X_path_for_Equities_BhavCopy,X_path_for_Security_wise_DeliveryPosition,X_path_for_Derivtives_BhavCopy,X_path_for_Derivtives_Label,Path_For_Folder_for_Number_of_Trades,file_Download_Folder,X_path_For_Body_text_SecurityWise):
    copy_file_object = CopyFileOperation.CopyFile()
    delete_object = Delete_Existing_Files.DeleteExistingFiles()
    Unzip_object = Unzip_operation.unzip()
    folder_object  = Xpath.FolderPath()
    delete_object.delete_Operation(folder_object.Folder_For_NumberOfTrades_CSV)
    time.sleep(5)
    chromeDriverObject = Xpath.ChromDriver()
    browser = Chrome(executable_path=chromeDriverObject.ChromDriverPath)
    browser.get(URL_to_Fetch_Data_for_NumberOfTrades)
    time.sleep(5)
    search_form = browser.find_element_by_xpath(X_path_for_Equities_BhavCopy)
    search_form.click()
    time.sleep(5)
    copy_file_object.Operation_To_Copy_File_From_One_Folder_To_Another_Folder(file_Download_Folder, Path_For_Folder_for_Number_of_Trades)
    time.sleep(5)
    Unzip_object.unzip_operation_for_NumberOfTrades()
    time.sleep(5)
    link = browser.find_element_by_xpath(X_path_for_Security_wise_DeliveryPosition)
    link.click()
    time.sleep(5)
    content = browser.find_element_by_xpath(X_path_For_Body_text_SecurityWise)
    securityData = content.text[287:]
    stock_data = str(securityData).split(',')
    #print(stock_data)
    stock_symbol_list = []
    stock_delievery_qty_list= []
    list_index_for_stock = 2
    list_index_for_delievery_qty = 5

    for list_index_for_stock in range(2,len(stock_data),6):
        stock_symbol_list.append(stock_data[list_index_for_stock])

    for list_index_for_delievery_qty in range(5,len(stock_data),6):
        stock_delievery_qty_list.append(stock_data[list_index_for_delievery_qty])

    combo_list = zip(stock_symbol_list,stock_delievery_qty_list)   #Using Zip Function to Combine two list and form one common list with two columns
    browser.execute_script("window.history.go(-1)")
    time.sleep(5)
    link = browser.find_element_by_xpath(X_path_for_Derivtives_Label)
    link.click()
    time.sleep(2)
    link = browser.find_element_by_xpath(X_path_for_Derivtives_BhavCopy)
    link.click()
    time.sleep(8)
    copy_file_object.Operation_To_Copy_File_From_One_Folder_To_Another_Folder(file_Download_Folder, Path_For_Folder_for_Number_of_Trades)
    Unzip_object.unzip_operation_for_NumberOfTrades()
    time.sleep(5)
    browser.close()
    return  list(combo_list)