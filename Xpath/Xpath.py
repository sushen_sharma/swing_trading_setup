class url:
    URL_to_Fetch_Data_for_Price_and_TradedVolume = "https://www1.nseindia.com/live_market/dynaContent/live_watch/equities_stock_watch.htm"
    URL_to_Fetch_Data_for_NumberOfTrades = "https://www1.nseindia.com/products/content/all_daily_reports.htm?param=equity"

class FolderPath:
    file_Download_Folder = "C:\\Users\\sushe\\Downloads\\*"
    Folder_For_Price_and_TradedVolume_zip = "D:\\Swing Trading Data\\Price_and_TradedVolume_zip"
    Folder_For_Price_and_TradedVolume_csv = "D:\\Swing Trading Data\\Price_and_TradedVolume_Csv"
    Folder_For_NumberOfTrades_zip = "D:\\Swing Trading Data\\Number_of_Trades_zip"
    Folder_For_NumberOfTrades_CSV = "D:\\Swing Trading Data\\Number_of_Trade_Csv"
class Xpath:
    X_path_For_DownloadXSV_To_Get_Price_and_TradedVolume ="//a[text()=\"Download in csv\"]"
    X_path_To_Select_FO_Stocks_FromDropDown="//select[@name=\"bankNiftySelect\"]/optgroup[@label=\"Others\"]/option[@value=\"foSec\"]"
    X_path_for_Equities_BhavCopy ="//tbody/tr/th/font[text()=\"Equities\"]/ancestor::tr/ancestor::tbody/tr[3]/td/a"
    X_path_for_Security_wise_DeliveryPosition ="//tbody/tr/th/font[text()=\"Equities\"]/ancestor::tr/ancestor::tbody/tr[7]/td/a"
    X_path_for_Derivtives_BhavCopy ="//tbody/tr/th/font[text()=\"Equity Derivatives\"]/ancestor::tr/ancestor::tbody/tr[3]/td/a"
    X_path_for_Derivtives_Label = "//li/a[text()='Derivatives']"
    X_path_For_Body_text_SecurityWise= "//body/pre"

class ChromDriver:
    ChromDriverPath = "C:\\Users\\sushe\\PycharmProjects\\repos\\SwingTradingSetup\\ChromeDriverExecutable\\chromedriver.exe"